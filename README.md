# INTERVIEW DECERTO #

INTERVIEW DECERTO Marek Marszelewski

To build and run the application, run the following commands:

``mvn clean package``

``mvn spring-boot:run``

After starting the application at http://localhost:8888/swagger-ui.html, there is swagger documentation.

The application provides two services, /configuration to download the configuration (Describes the operation you can perform and on what data), 
and the second service /calculate, that performs the indicated operation on specific data.

Configuration: (example)

`curl --location --request GET 'http://localhost:8888/configuration'`

Calculate: (example)

`curl --location --request POST 'http://localhost:8888/calculate' --header 'Content-Type: application/json' --data-raw '{"operation": "ADDING_INTEGERS", "sources": ["JAVA_RANDOM_INTEGER", "API_CLIENT_RANDOM_ORG_INTEGER"]}'`
