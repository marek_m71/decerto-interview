CREATE TABLE operations
(
    id         BIGSERIAL PRIMARY KEY,
    operation  varchar(255)
);

CREATE TABLE sources
(
    id         BIGSERIAL PRIMARY KEY,
    source     varchar(255)
);

