package com.decerto.interview.dto.in;

import com.decerto.interview.db.enums.OperationEnum;
import com.decerto.interview.db.enums.SourceEnum;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class CalculateRequest {

  @NotNull
  private OperationEnum operation;
  @NotNull
  @Size(min = 1)
  private List<SourceEnum> sources;
}
