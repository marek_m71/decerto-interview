package com.decerto.interview.dto.provider.out;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ClientDataResponse {

  private List<Integer> data = null;
  private String completionTime;
}
