package com.decerto.interview.dto.provider.in;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class ClientParamsRequest {

  private String apiKey;
  private Integer n;
  private Integer min;
  private Integer max;
  private Boolean replacement;
}
