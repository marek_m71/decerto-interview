package com.decerto.interview.dto.provider.out;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ClientResponse {

  private String jsonrpc;
  private ClientResultResponse result;
  private Integer id;
}
