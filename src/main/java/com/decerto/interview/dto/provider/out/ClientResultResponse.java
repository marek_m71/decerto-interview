package com.decerto.interview.dto.provider.out;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ClientResultResponse {

  private ClientDataResponse random;
  private Integer bitsUsed;
  private Integer bitsLeft;
  private Integer requestsLeft;
  private Integer advisoryDelay;
}
