package com.decerto.interview.dto.provider.in;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class ClientRequest {

  private String jsonrpc;
  private String method;
  private ClientParamsRequest params;
  private Integer id;
}
