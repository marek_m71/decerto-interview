package com.decerto.interview.dto.out;

import com.decerto.interview.db.enums.OperationEnum;
import com.decerto.interview.db.enums.SourceEnum;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class ConfigurationResponse {

  private List<OperationEnum> operations;
  private List<SourceEnum> sources;
}
