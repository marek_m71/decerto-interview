package com.decerto.interview.dto.out;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class CalculateResponse {

  private Integer result;
}
