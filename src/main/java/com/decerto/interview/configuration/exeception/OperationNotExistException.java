package com.decerto.interview.configuration.exeception;

import com.decerto.interview.db.enums.OperationEnum;

public class OperationNotExistException extends RuntimeException {

  public OperationNotExistException(OperationEnum operation) {
    super("Operation " + operation + " doesn't exist");
  }
}
