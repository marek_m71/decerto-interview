package com.decerto.interview.configuration.exeception.advices;

import com.decerto.interview.configuration.exeception.OperationNotExistException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;
import org.zalando.problem.spring.web.advice.AdviceTrait;

public interface OperationNotExistAdviceTrait extends AdviceTrait {

  @ExceptionHandler
  default ResponseEntity<Problem> operationNotExistException(final OperationNotExistException e,
      final NativeWebRequest request) {
    Logger logger = LoggerFactory.getLogger(OperationNotExistException.class);

    if (logger.isErrorEnabled()) {
      logger.error("Operation Not Exist, message: {}", e.getMessage());
    }

    return create(Status.INTERNAL_SERVER_ERROR, e, request);
  }
}
