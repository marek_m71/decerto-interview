package com.decerto.interview.configuration.exeception.handler;

import com.decerto.interview.configuration.exeception.advices.FeignClientErrorAdviceTrait;
import com.decerto.interview.configuration.exeception.advices.OperationNotExistAdviceTrait;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.zalando.problem.spring.web.advice.ProblemHandling;

@ControllerAdvice
class ExceptionHandling implements ProblemHandling, FeignClientErrorAdviceTrait,
    OperationNotExistAdviceTrait {

}

