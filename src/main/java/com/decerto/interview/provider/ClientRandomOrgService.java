package com.decerto.interview.provider;

import com.decerto.interview.dto.provider.in.ClientParamsRequest;
import com.decerto.interview.dto.provider.in.ClientRequest;
import com.decerto.interview.dto.provider.out.ClientResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ClientRandomOrgService {

  @Value("${client.api.key}")
  private String apiKey;

  private final ClientRandomOrg clientRandomOrg;

  private static final String GENERATE_INTEGER = "generateIntegers";
  private static final String JSON_RPC = "2.0";
  private static final Integer GENERATE_INTEGER_ID = 1;
  private static final Integer GENERATE_INTEGER_MIN = 0;
  private static final Integer GENERATE_INTEGER_MAX = 1000;
  private static final Integer GENERATE_INTEGER_N = 100;

  public ClientRandomOrgService(ClientRandomOrg clientRandomOrg) {
    this.clientRandomOrg = clientRandomOrg;
  }

  public Integer generateIntegers() {
    ClientResponse response = clientRandomOrg.execute(buildRandomRequest());

    return response.getResult().getRandom().getData().stream().findFirst().orElse(0);
  }

  private ClientRequest buildRandomRequest() {
    return ClientRequest.builder()
        .id(GENERATE_INTEGER_ID)
        .method(GENERATE_INTEGER)
        .jsonrpc(JSON_RPC)
        .params(buildRandomParamsRequest())
        .build();
  }

  private ClientParamsRequest buildRandomParamsRequest() {
    return ClientParamsRequest.builder()
        .apiKey(apiKey)
        .n(GENERATE_INTEGER_N)
        .min(GENERATE_INTEGER_MIN)
        .max(GENERATE_INTEGER_MAX)
        .replacement(true)
        .build();
  }
}
