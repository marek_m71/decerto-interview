package com.decerto.interview.provider;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.decerto.interview.dto.provider.in.ClientRequest;
import com.decerto.interview.dto.provider.out.ClientResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "Client", url = "${client.api.url}")
public interface ClientRandomOrg {

  @PostMapping(produces = APPLICATION_JSON_VALUE)
  ClientResponse execute(@RequestBody ClientRequest request);
}
