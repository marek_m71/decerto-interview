package com.decerto.interview.service;

import com.decerto.interview.configuration.exeception.OperationNotExistException;
import com.decerto.interview.dto.in.CalculateRequest;
import com.decerto.interview.dto.out.CalculateResponse;
import com.decerto.interview.service.operation.OperationService;
import com.decerto.interview.service.sources.SourceService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class CalculateService {

  private final List<OperationService> operationServiceList;
  private final List<SourceService> sourceServices;

  public CalculateService(List<OperationService> operationServiceList,
      List<SourceService> sourceServices) {
    this.operationServiceList = operationServiceList;
    this.sourceServices = sourceServices;
  }

  public CalculateResponse calculate(CalculateRequest request) {
    List<Integer> values = sourceServices.stream()
        .filter(r -> request.getSources().contains(r.getSource()))
        .map(SourceService::getValue)
        .collect(Collectors.toList());

    return CalculateResponse.builder()
        .result(operationServiceList.stream()
            .filter(r -> r.getOperationType().equals(request.getOperation()))
            .findFirst().orElseThrow(() -> new OperationNotExistException(request.getOperation()))
            .calculate(values))
        .build();
  }
}
