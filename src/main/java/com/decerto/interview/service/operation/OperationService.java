package com.decerto.interview.service.operation;

import com.decerto.interview.db.enums.OperationEnum;
import java.util.List;

public interface OperationService {

  Integer calculate(List<Integer> values);

  OperationEnum getOperationType();
}
