package com.decerto.interview.service.operation.impl;

import com.decerto.interview.db.enums.OperationEnum;
import com.decerto.interview.service.operation.OperationService;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class AdditionOperationServiceImpl implements OperationService {

  @Override
  public Integer calculate(List<Integer> values) {
    return values.stream().mapToInt(Integer::intValue).sum();
  }

  public OperationEnum getOperationType() {
    return OperationEnum.ADDING_INTEGERS;
  }
}
