package com.decerto.interview.service;

import com.decerto.interview.db.Operation;
import com.decerto.interview.db.Source;
import com.decerto.interview.dto.out.ConfigurationResponse;
import com.decerto.interview.repository.OperationRepository;
import com.decerto.interview.repository.SourceRepository;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class ConfigurationService {

  private final OperationRepository operationRepository;
  private final SourceRepository sourceRepository;

  public ConfigurationService(OperationRepository operationRepository,
      SourceRepository sourceRepository) {
    this.operationRepository = operationRepository;
    this.sourceRepository = sourceRepository;
  }

  public ConfigurationResponse getConfiguration() {
    return ConfigurationResponse.builder()
        .operations(operationRepository.findAll().stream().map(Operation::getOperationEnum)
            .collect(Collectors.toList()))
        .sources(sourceRepository.findAll().stream().map(Source::getSourceEnum)
            .collect(Collectors.toList()))
        .build();
  }
}
