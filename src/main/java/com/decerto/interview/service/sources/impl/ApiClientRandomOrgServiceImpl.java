package com.decerto.interview.service.sources.impl;

import com.decerto.interview.db.enums.SourceEnum;
import com.decerto.interview.provider.ClientRandomOrgService;
import com.decerto.interview.service.sources.SourceService;
import org.springframework.stereotype.Service;

@Service
public class ApiClientRandomOrgServiceImpl implements SourceService {

  private final ClientRandomOrgService clientRandomOrgService;

  public ApiClientRandomOrgServiceImpl(ClientRandomOrgService clientRandomOrgService) {
    this.clientRandomOrgService = clientRandomOrgService;
  }

  @Override
  public Integer getValue() {
    return clientRandomOrgService.generateIntegers();
  }

  @Override
  public SourceEnum getSource() {
    return SourceEnum.API_CLIENT_RANDOM_ORG_INTEGER;
  }
}
