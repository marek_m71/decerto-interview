package com.decerto.interview.service.sources;

import com.decerto.interview.db.enums.SourceEnum;

public interface SourceService {

  Integer getValue();

  SourceEnum getSource();
}
