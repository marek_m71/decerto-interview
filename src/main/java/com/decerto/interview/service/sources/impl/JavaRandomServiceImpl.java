package com.decerto.interview.service.sources.impl;

import com.decerto.interview.db.enums.SourceEnum;
import com.decerto.interview.service.sources.SourceService;
import java.util.Random;
import org.springframework.stereotype.Service;

@Service
public class JavaRandomServiceImpl implements SourceService {

  private final static int MAX = 1000;

  @Override
  public Integer getValue() {
    return new Random().nextInt(MAX);
  }

  @Override
  public SourceEnum getSource() {
    return SourceEnum.JAVA_RANDOM_INTEGER;
  }
}
