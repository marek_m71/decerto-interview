package com.decerto.interview.controller;

import com.decerto.interview.dto.out.ConfigurationResponse;
import com.decerto.interview.service.ConfigurationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("configuration")
public class ConfigurationController {

  private final ConfigurationService configurationService;

  public ConfigurationController(ConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  @GetMapping
  public ResponseEntity<ConfigurationResponse> getConfiguration() {
    return ResponseEntity.ok(configurationService.getConfiguration());
  }
}
