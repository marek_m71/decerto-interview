package com.decerto.interview.controller;

import com.decerto.interview.dto.in.CalculateRequest;
import com.decerto.interview.dto.out.CalculateResponse;
import com.decerto.interview.service.CalculateService;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("calculate")
public class CalculateController {

  private final CalculateService calculateService;

  public CalculateController(CalculateService calculateService) {
    this.calculateService = calculateService;
  }

  @PostMapping
  public ResponseEntity<CalculateResponse> calculate(@RequestBody @Valid CalculateRequest request) {
    return ResponseEntity.ok(calculateService.calculate(request));
  }
}
