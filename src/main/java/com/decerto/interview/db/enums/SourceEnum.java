package com.decerto.interview.db.enums;

public enum SourceEnum {
  JAVA_RANDOM_INTEGER,
  API_CLIENT_RANDOM_ORG_INTEGER
}
