package com.decerto.interview.service;

import com.decerto.interview.dto.out.ConfigurationResponse;
import java.util.stream.Collectors;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConfigurationServiceTest {

  private static final String ADDING_INTEGERS = "ADDING_INTEGERS";
  private static final String JAVA_RANDOM_INTEGER = "JAVA_RANDOM_INTEGER";

  @Autowired
  private ConfigurationService configurationService;

  @Test
  public void configurationShouldContainsOperation() {
    ConfigurationResponse response = configurationService.getConfiguration();

    Assert.assertTrue(
        response.getOperations().stream().map(Enum::toString).collect(Collectors.toList())
            .contains(ADDING_INTEGERS));
  }

  @Test
  public void configurationShouldContainsSources() {
    ConfigurationResponse response = configurationService.getConfiguration();

    Assert.assertTrue(
        response.getSources().stream().map(Enum::toString).collect(Collectors.toList())
            .contains(JAVA_RANDOM_INTEGER));
  }
}
